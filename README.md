# Installing HedgeDoc+SSL with docker-compose.

## Quick Installation

**Before starting the instance, direct the domain (subdomain) to the ip of the server where HedgeDoc will be installed!**

## 1. HedgeDoc Server Requirements
From and more
- 1 CPUs
- 1 RAM 
- 10 Gb 

Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/hedgedoc-ssl-docker-compose/-/raw/master/setup.sh | sudo bash -s

```

Download instance:

``` bash
curl -s ttps://gitlab.com/6ministers/business-apps/hedgedoc-ssl-docker-compose/-/raw/master/download.sh | sudo bash -s hedgedoc
```

If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd hedgedoc
```

To change the domain in the `Caddyfile` to your own

``` bash
#Hedgedoc
https://hedgedoc.domains.com:443 {
    reverse_proxy 127.0.0.1:3000
	# tls admin@example.org
	encode zstd gzip
	file_server
...	
}
```

Add a domain to ``.env`
``` bash
DOMAIN=hedgedoc.6ministers.com
```

**Run apps:**

``` bash
docker-compose up -d
```

Then open 
`https://hedgedoc.domain.com`

## Container management

**Run onlyoffice**:

``` bash
docker-compose up -d
```

**Restart**:

``` bash
docker-compose restart
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

**Stop**:

``` bash
docker-compose down
```

## Documentation

https://hedgedoc.org/

https://docs.hedgedoc.org/setup/getting-started/


